#ifndef COMMONACTIONS_HPP
#define COMMONACTIONS_HPP

#include <string>
#include <memory>

#include <OpenXLSX/OpenXLSX.h>

class WorksheetActions
{
public:


    ///From worksheet name to index
    inline static int fromNameToIndex(std::shared_ptr<OpenXLSX::XLDocument> document,
                                      std::string name)
    {
        return document->Workbook().Worksheet(name).Index();
    }


    ///From worksheet index to name.
    inline static std::string fromIndexToName(std::shared_ptr<OpenXLSX::XLDocument> document,
                                              int index)
    {
       return document->Workbook().WorksheetNames().at(index);
    }


private:
    WorksheetActions();


};


#endif // COMMONACTIONS_HPP
