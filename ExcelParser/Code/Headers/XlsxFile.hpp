#ifndef XLSXFILE_HPP
#define XLSXFILE_HPP

#include <string>
#include <memory>

#include <OpenXLSX/OpenXLSX.h>

class XlsxFile
{
public:
    XlsxFile(std::string full_name);

    std::shared_ptr <OpenXLSX::XLDocument> getDocument();

    OpenXLSX::XLWorksheet &getActiveWorksheet();

    int getTotalRows();
    int getTotalColumns();


private:
    std::string full_name = "No full path";
    std::string file_name = "Unnamed File";
    std::string worksheet_document = "Unnamed worksheet";

    std::shared_ptr<OpenXLSX::XLDocument> document = nullptr;

    std::shared_ptr<OpenXLSX::XLWorksheet> active_worksheet = nullptr;


    void initFile();

    bool fileIsValid();

    std::string fileNameFromFullPath(std::string full_path);



};

#endif // XLSXFILE_HPP
