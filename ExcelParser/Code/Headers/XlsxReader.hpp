#ifndef XLSXREADER_HPP
#define XLSXREADER_HPP

#include <string>
#include <memory>

#include <XlsxFile.hpp>


class XlsxReader
{
public:
    XlsxReader(std::shared_ptr<XlsxFile> file);
    XlsxReader(std::string path);

    inline std::string readCell(uint32_t row, uint32_t column, std::string workbook_name = "")
    {
        if (workbook_name == "")
        {
            workbook_name = this->file->getDocument()->Workbook().WorksheetNames()[0];
            this->selected_worksheet = workbook_name;
        }

        else
        {
            ///TODO if a workbook name is specified look for the cells in it.

        }

        return this->file->getDocument()->Workbook().Worksheet(selected_worksheet).Cell(row, column).Value().AsString();
    }

    std::string getFirstWorksheetId();

    std::shared_ptr <XlsxFile> getFile();


private:
    std::string selected_worksheet = "";

    std::shared_ptr<XlsxFile> file = nullptr;


};

#endif // XLSXREADER_HPP
