#ifndef XLSXWRITER_HPP
#define XLSXWRITER_HPP

#include <string>
#include <memory>

#include <XlsxFile.hpp>

class XlsxWriter
{
public:
    XlsxWriter(std::shared_ptr<XlsxFile> file);

    void writeAt(int row, int column, std::string content);


private:
    std::shared_ptr<XlsxFile> file = nullptr;

};

#endif // XLSXWRITER_HPP
