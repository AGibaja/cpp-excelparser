#include <XlsxFile.hpp>

#include <iostream>
#include <cassert>

#include <XlsxReader.hpp>

XlsxFile::XlsxFile(std::string full_name)
    : full_name (full_name)
{
    this->initFile();
}


std::shared_ptr <OpenXLSX::XLDocument> XlsxFile::getDocument()
{
    return this->document;
}


OpenXLSX::XLWorksheet &XlsxFile::getActiveWorksheet()
{
    return *this->active_worksheet;
}


int XlsxFile::getTotalRows()
{
    auto row_count = this->getActiveWorksheet().RowCount();
    return row_count;
}


int XlsxFile::getTotalColumns()
{
    auto column_count = this->getActiveWorksheet().ColumnCount();
    return column_count;
}


void XlsxFile::initFile()
{
    this->file_name = this->fileNameFromFullPath(this->full_name);
    this->document = std::make_shared<OpenXLSX::XLDocument>();

    if (!this->fileIsValid())
    {
        assert (this->fileIsValid() && "File is invalid.");
        return;
    }

    else
    {
        try
        {
            this->document->OpenDocument(this->file_name);
            std::string wk = this->document->Workbook().WorksheetNames()[0];
            this->active_worksheet = std::make_shared<OpenXLSX::XLWorksheet>
                    (this->document->Workbook().Worksheet(wk));
        }

        catch (std::exception &e)
        {
            std::cout << e.what() << std::endl;
            return;
        }
    }
}


///TO DO: check if a file is an xlsx file.
bool XlsxFile::fileIsValid()
{
    return true;
}


std::string XlsxFile::fileNameFromFullPath(std::string full_path)
{
    int slash_position = 0;
    for (int i = full_path.length() ; i > -1; --i)
    {
        if (full_path[i] == '/')
        {
            slash_position = i + 1;
            break;
        }
    }
    return full_path.substr(slash_position, full_path.length());
}
