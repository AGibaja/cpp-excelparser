#include <XlsxReader.hpp>

#include <iostream>
#include <cassert>
#include <stdio.h>
#include <string.h>

#include <XlsxFile.hpp>


XlsxReader::XlsxReader(std::shared_ptr<XlsxFile> file)
    : file(file)
{

}


std::string XlsxReader::getFirstWorksheetId()
{
    return this->selected_worksheet;
}


std::shared_ptr<XlsxFile> XlsxReader::getFile()
{
    return this->file;
}
