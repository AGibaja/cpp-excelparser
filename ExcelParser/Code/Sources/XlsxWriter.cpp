#include <XlsxWriter.hpp>

#include <iostream>

XlsxWriter::XlsxWriter(std::shared_ptr<XlsxFile> file)
    : file (file)
{
    std::cout << "Writer instantiated" << std::endl;

}


void XlsxWriter::writeAt(int row, int column, std::string contents)
{
    std::cout << "Writing..." << std::endl;
    try
    {
        this->file->getActiveWorksheet().Cell(row, column).Value().Set(contents);
        this->file->getDocument()->SaveDocument();
    }

    catch (std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }

    std::cout << "Written..." << std::endl;
}
