QT -= gui

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Code/Sources/XlsxFile.cpp \
    Code/Sources/XlsxReader.cpp \
    Code/Sources/XlsxWriter.cpp

HEADERS += \
    Code/Headers/CommonActions.hpp \
    Code/Headers/XlsxFile.hpp \
    Code/Headers/XlsxReader.hpp \
    Code/Headers/XlsxWriter.hpp

INCLUDEPATH += \
    ./Code/Headers/
    ./Libraries/OpenXlslx/install/include/ \


# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/Libraries/OpenXlslx/install/lib/release/ -lOpenXLSX
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/Libraries/OpenXlslx/install/lib/debug/ -lOpenXLSX
else:unix: LIBS += -L$$PWD/Libraries/OpenXlslx/install/lib/ -lOpenXLSX

INCLUDEPATH += $$PWD/Libraries/OpenXlslx/install/include
DEPENDPATH += $$PWD/Libraries/OpenXlslx/install/include
